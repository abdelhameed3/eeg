/*global $ */
/*eslint-env browser*/


//======================== Loading =========================//
$(document).ready(function () {
  "use strict";
  $(window).on('load', function () {
      $(".load").fadeOut(100, function () {
          $("body").css("overflow", "auto");
          $(this).fadeOut(100, function () {
              $(this).remove();
          });
      });
  });


  /* Popup video */
  $(".js-modal-btn").modalVideo();

});
//==================== Slider ==============================//
$(document).ready(function () {
    'use strict';
  $('.main-widget').click(function () {
    $(this).addClass('active').siblings().removeClass('active');
  });

  $('.step-slider').slick({
    rtl: true,
    nextArrow:'.next',
    prevArrow:'.prev',
    loop:false,
    draggable:false

  });
  $('.slider-app .slick').slick({
    rtl: true,
    nextArrow:'.app-control .prev',
    prevArrow:'.app-control .next',
    infinite:false,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        }
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  $('.gallery').slick({
    rtl: true,
    nextArrow:'.image-gallery .app-control .prev',
    prevArrow:'.image-gallery .app-control .next',
    infinite:false,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        }
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 550,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
  $('.vacances .slider').slick({
    rtl: true,
    nextArrow:'.vacances .app-control .prev',
    prevArrow:'.vacances .app-control .next',
    infinite:false,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.links .slider').slick({
    rtl: true,
    nextArrow:'.links .app-control .prev',
    prevArrow:'.links .app-control .next',
    infinite:false,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.blog-slider,.blogs-widget').slick({
    dots:true,
    arrows:false,
    infinite:true,
    rtl: true,
   
  })
   
  //====================== Lightgallery ======================//
lightGallery(document.getElementById('gallery'), {
  selector: '.image'
}); 

  });



   
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("item-step");
  x[n].style.display = "block";
  
  
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("item-step");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("item-step");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == ""  )  {
      // add an "invalid" class to the field:
      y[i].className = " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  
  return valid; // return the valid status
}
